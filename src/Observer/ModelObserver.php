<?php


namespace ShipIT\EloquentUuidable\Observer;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ModelObserver
{
    const UUID_FIELD = 'uuid';

    public function creating(Model $model): void
    {
        if ($model->getAttribute(self::UUID_FIELD)) {
            return;
        }

        do {
            $uuid = Str::uuid();
        } while (!$this->isUnique($uuid, $model));

        $model->setAttribute(self::UUID_FIELD, $uuid);
    }

    private function isUnique(string $uuid, Model $model): bool
    {
        return $model->newQuery()->where(self::UUID_FIELD, $uuid)->doesntExist();
    }
}
