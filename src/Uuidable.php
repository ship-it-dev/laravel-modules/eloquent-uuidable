<?php


namespace ShipIT\EloquentUuidable;


use ShipIT\EloquentUuidable\Observer\ModelObserver;

trait Uuidable
{
    public static function bootUuidable()
    {
        self::observe(ModelObserver::class);
    }
}
